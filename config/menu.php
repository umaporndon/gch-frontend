<?php

/*
|--------------------------------------------------------------------------
| Menu items
|--------------------------------------------------------------------------
|
| Example for child menus:
| 'childMenu' => [
|     [
|         'routeName'  => '',
|         'parameters' => [], (Route's parameters. This is optional.)
|         'menuText'   => '', (Reference translation string)
|         'class'      => '', (Use class attribute as a class which has authorization policy; if skip this attribute, it means that the menu always show this menu choice. )
|         'gate'       => '', (Use gate attribute as an authorization method. Default is 'view' method. )
|     ],
| ],
|
*/

return [
    'mainMenu' => [
        [
            'routeName' => 'dashboard.index',
            'menuText'  => 'dashboard.page_link.index',
        ],
        [
            'routeName' => 'wallet.index',
            'menuText'  => 'wallet.page_link.index',
        ],
        [
            'routeName' => 'project-info.index',
            'menuText'  => 'project-info.page_link.index',
        ],
        [
            'routeName' => 'knowledge-base.index',
            'menuText'  => 'knowledge-base.page_link.index',
        ],
        [
            'routeName' => 'profile.index',
            'menuText'  => 'profile.page_link.index',
        ],
        [
            'routeName' => 'organization.index',
            'menuText'  => 'organization.page_link.index',
        ],
    ],
];