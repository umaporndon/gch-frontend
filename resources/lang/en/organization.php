<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Organization Page Language Lines
    |--------------------------------------------------------------------------
    */

    'page_link' => [
        'index' => "<img src=" . asset('images/menu/organization.svg') . " width=20>" . ' Organizations',
    ],

    'page_title' => [
        'index' => 'Organization',
    ],

    'page_description' => [
        'index' => 'Organization page',
    ],

    'page_heading' => [
        'index' => 'Organization',
    ],


];