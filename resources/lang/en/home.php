<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Home Page Language Lines
    |--------------------------------------------------------------------------
    */

    'page_link' => [
        'index' => '',
    ],

    'page_title' => [
        'index' => '',
    ],

    'page_description' => [
        'index' => ' ',
    ],

    'page_keyword' => [
        'index' => '',
    ],

    'og_title' => [
        'index' => '',
    ],

    'og_description' => [
        'index' => '',
    ],

    'og_keyword' => [
        'index' => '',
    ],

    'og_url' => [
        'index' => route( 'home.index' ),
    ],

];
