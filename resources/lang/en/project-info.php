<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Project Info Page Language Lines
    |--------------------------------------------------------------------------
    */

    'page_link' => [
        'index' => "<img src=" . asset('images/menu/project-info.svg') . " width=20>" . ' Project Infos',
    ],

    'page_title' => [
        'index' => 'Project Info',
    ],

    'page_description' => [
        'index' => 'Project Info page',
    ],

    'page_heading' => [
        'index' => 'Project Info',
    ],


];