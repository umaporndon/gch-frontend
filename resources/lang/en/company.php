<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Company Page Language Lines
    |--------------------------------------------------------------------------
    */

    'page_link' => [
        'index' => 'Company Management',
    ],

    'page_title' => [
        'index'   => 'Company Management',
        'create'  => 'Create a new company.',
        'edit'    => 'Edit company',
        'profile' => 'Profile',
    ],

    'page_description' => [
        'index'   => 'Company Management',
        'create'  => 'Create a new company.',
        'edit'    => 'Edit company',
        'profile' => 'Company Profile',
    ],

    'page_heading' => [
        'index'   => 'Company Management',
        'create'  => 'Create a new company.',
        'edit'    => 'Edit company',
        'profile' => 'Profile',
    ],

    'links' => [
        'profile' => 'Go to your profile page',
    ],

    'email'                 => 'E-Mail Address',
    'password'              => 'Password',
    'password_confirmation' => 'Confirm Password',
    'firstName'             => 'First Name',
    'lastName'              => 'Last Name',
    'middleName'            => 'Middle Name',
    'mobile'                => 'Mobile Number',
    'avatar'                => 'Avatar',
    'saved_company_success' => 'Company information saved successfully.',
    'saved_company_error'   => 'Company information was not saved successfully.',
];
