<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Knowledge base Page Language Lines
    |--------------------------------------------------------------------------
    */

    'page_link' => [
        'index' => "<img src=" . asset('images/menu/knowledge.svg') . " width=20>" . ' Knowledge bases',
    ],

    'page_title' => [
        'index' => 'Knowledge base',
    ],

    'page_description' => [
        'index' => 'Knowledge base page',
    ],

    'page_heading' => [
        'index' => 'Knowledge base',
    ],


];