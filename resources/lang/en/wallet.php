<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Wallet Page Language Lines
    |--------------------------------------------------------------------------
    */

    'page_link' => [
        'index' => "<img src=" . asset('images/menu/wallet.svg') . " width=20>" . ' Wallet',
    ],

    'page_title' => [
        'index' => 'Wallet',
    ],

    'page_description' => [
        'index' => 'Wallet page',
    ],

    'page_heading' => [
        'index' => 'Wallet',
    ],


];