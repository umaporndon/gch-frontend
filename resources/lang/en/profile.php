<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Profile Page Language Lines
    |--------------------------------------------------------------------------
    */

    'page_link' => [
        'index' => "<img src=" . asset('images/menu/profile.svg') . " width=20>" . ' Profiles',
    ],

    'page_title' => [
        'index' => 'Profile',
    ],

    'page_description' => [
        'index' => 'Profile page',
    ],

    'page_heading' => [
        'index' => 'Profile',
    ],


];