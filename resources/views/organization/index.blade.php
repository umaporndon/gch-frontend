@extends('layouts.app')

@section('page-title', __('organization.page_title.index'))
@section('page-description', __('organization.page_description.index'))

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                @include('layouts.menu')
            </div>
            <div class="col-xs-12 col-md-9">
                <div class="p-2">
                    <div class="page-title">Organization</div>
                    <div class="row pt-2">
                        <div class="col-xs-12 col-md-3">
                            <div class="profile-thumb">

                                <img src="{{( $company->isNotEmpty() ) ? $company[0]->image_path : asset('images/avatar.png') }}" id="preview">
                                <span style="cursor:pointer">
                                    <a class="profile-upload"><i class="fa fa-camera"></i></a>
                                </span>
                                <!-- Modal -->
                                <div class="modal fade bd-example-modal-xl" id="dialog-upload-image" tabindex="-1" role="dialog" aria-labelledby="upload-imageTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="dialog-upload-imageTitle">Image
                                                                                                      preview</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-4 text-center">
                                                        <div id="upload-demo"></div>
                                                    </div>
                                                    <div class="col-sm-4" style="padding:5%;">
                                                        <strong>Select image to crop:</strong>
                                                        <input type="file" id="image">
                                                        <button class="btn btn-success btn-block btn-upload-image"
                                                                style="margin-top:2%"
                                                                data-url="{{ route('organization.update-company-image' ) }}">
                                                            Upload Image
                                                        </button>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div id="preview-crop-image" style="background:#8484841f;width:300px;padding:70px 80px;height:300px;"></div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <form action="{{ route('organization.update-company-image-path' ) }}"
                                                      method="POST"
                                                      class="submission-form">
                                                    <div class="hide-input"></div>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                        Close
                                                    </button>
                                                    <button type="submit" class="btn btn-primary">Save changes</button>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-9 profile-detail">
                            <div class="text-bold"> {{ ( $company->isNotEmpty() ) ? $company[0]->company_name : 'Company Name' }} </div>
                            <div>{{ ( $company->isNotEmpty() ) ? $company[0]->email : 'Email Address' }}</div>
                            <div><span class="text-bold">Wallet Secret:</span> ***********</div>
                            <div><span class="text-bold">Wallet ID:</span> 29sj9a0</div>
                        </div>
                    </div>
                    <div>
                        <h5 class="pt-4 text-bold border-bottom-blue">Company Information</h5>
                        <form action="{{ route('organization.create' ) }}" method="POST" class="submission-form">

                            {{ csrf_field() }}

                            <div class="profile-form col-8">
                                <div class="profile-input">
                                    <label for="company" class="form-label">Company</label>
                                    <div>
                                        <input type="text" class="form-control"
                                               value="{{ ( $company->isNotEmpty() ) ? $company[0]->company_name : '' }}"
                                               placeholder="Company" name="company_name" id="company_name">
                                        <p id="company_name-help-text" class="help-text help-text none"></p>
                                    </div>
                                </div>
                                <div class="profile-input">
                                    <label for="address" class="form-label">Address</label>
                                    <div>
                                        <textarea class="form-control" placeholder="Address" name="address">
                                            {{ ( $company->isNotEmpty() ) ? $company[0]->address : '' }}
                                        </textarea>
                                    </div>
                                </div>
                                <script src="js/city.js"></script>

                                <div class="profile-input row">
                                    <form>
                                        <div class="col-6">
                                            <label for="country" class="form-label">Country</label>
                                            <select class="form-select form-control" name="country" id="country"
                                                    onchange="set_city_state(this,city_state)">
                                                <option>Open this select country</option>
                                                <script type="text/javascript">setRegions( "{{ ( $company->isNotEmpty() ) ? $company[0]->country : '' }}" );</script>
                                            </select>
                                            <p id="country-help-text" class="help-text help-text none"></p>
                                        </div>
                                        <div class="col-6">
                                            <label for="state" class="form-label">State/City</label>
                                            <select onchange="print_city_state(country,this)" class="form-select form-control" name="city_state" {{--name="state" id="state"--}}>
                                                <option>Open this select state/city</option>
                                                @if( ( $company->isNotEmpty() ) )
                                                    <option value="{{ $company[0]->state }}" selected>{{$company[0]->state}}</option>
                                                @endif
                                            </select>
                                            <p id="state-help-text" class="help-text help-text none"></p>
                                        </div>
                                        <div id="txtregion" class="d-none"></div>
                                        <div id="txtplacename" class="d-none"></div>
                                    </form>
                                </div>
                                <div class="profile-input row">
                                    {{-- <div class="col-6">
                                         <label for="country" class="form-label">City</label>
                                         <select class="form-select form-control">
                                             <option selected>Open this select country</option>
                                             <option value="1">One</option>
                                             <option value="2">Two</option>
                                             <option value="3">Three</option>
                                         </select>
                                     </div>--}}
                                    <div class="col-6">
                                        <label for="country" class="form-label">Postal Code</label>
                                        <input type="text" class="form-control" name="postcode"
                                               value="{{ ( $company->isNotEmpty() ) ? $company[0]->postcode : '' }}"
                                               placeholder="Postal Code">
                                    </div>
                                </div>
                                <div class="profile-input row">
                                    <div class="col-6">
                                        <label for="country" class="form-label">Phone</label>
                                        <input type="text" class="form-control" name="phone"
                                               value="{{ ( $company->isNotEmpty() ) ? $company[0]->phone : '' }}"
                                               placeholder="Phone">
                                    </div>
                                    <div class="col-6">
                                        <label for="country" class="form-label">Mobile</label>
                                        <input type="text" class="form-control"
                                               value="{{ ( $company->isNotEmpty() ) ? $company[0]->mobile : '' }}"
                                               name="mobile" placeholder="Mobile">
                                    </div>
                                </div>
                                <div class="profile-input">
                                    <label for="title" class="form-label">Email</label>
                                    <div>
                                        <input type="text" class="form-control"
                                               value="{{ ( $company->isNotEmpty() ) ? $company[0]->email : '' }}"
                                               placeholder="Email Address (e.g. john@example.com)" name="email" id="email">
                                        <p id="email-help-text" class="help-text help-text none"></p>
                                    </div>
                                </div>
                                <div class="profile-input">
                                    <label for="title" class="form-label">Second Email</label>
                                    <div>
                                        <input type="text" class="form-control"
                                               value="{{ ( $company->isNotEmpty() ) ? $company[0]->second_email : '' }}"
                                               placeholder="Email Address (e.g. john@example.com)l" name="second_semail">
                                    </div>
                                </div>
                                <div class="profile-input">
                                    <label for="title" class="form-label">Website</label>
                                    <div>
                                        <input type="text" class="form-control"
                                               value="{{ ( $company->isNotEmpty() ) ? $company[0]->website : '' }}"
                                               placeholder="Website Address (e.g. www.sample.com)" name="website">
                                    </div>
                                </div>
                                <div class="kyc-box">
                                    <div class="text-bold">KYC/AML Status</div>
                                    <i class="fa fa-check"></i>Individual
                                </div>
                                <div class="profile-input mt-3">
                                    <div class="input-group">
                                        <button type="submit" class="input-group-text button-blue text-bold p-2 w-50">
                                            Change
                                        </button>
                                        {{--                                    <button class="input-group-text button-blue text-bold p-2 w-50">START KYC/AML NOW--}}
                                        {{--                                    </button>--}}
                                    </div>
                                </div>
                            </div>
                        </form>
                        <h5 class="pt-5 text-bold border-bottom-blue">Pending Request</h5>
                        <div class="pending-request-list">
                            @include('organization.list')
                        </div>
                        <h5 class="pt-5 text-bold border-bottom-blue">Company Members</h5>

                        @forelse($companyMember as $memberItem)
                            <div class="col-8 p-3">
                                <div class="row align-items-center">
                                    <div class="col-sm-2">
                                        <img src="{{ $memberItem['image_path'] ? $memberItem['image_path'] : asset('images/avatar.png') }}" class="list-thumb">
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="text-bold">{{ $memberItem['firstname'] . ' ' . $memberItem['lastname'] }}</div>
                                        <div>{{ $memberItem['country'] }}</div>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="hidden" id="fk_user_id" name="fk_user_id" value="{{ $memberItem['id'] }}">
                                        <input type="hidden" id="userCompanyidID" name="id" value="{{ $memberItem['userCompanyID'] }}">
                                        <input type="hidden" id="status" name="status" value="revoke">
                                        <button onclick="return false;" data-url="{{ route('organization.action-to-join') }}" class="input-group-text button-red text-bold action-to-join">
                                            Revoke
                                        </button>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <div class="pending-box">Not found member.</div>
                        @endforelse

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
