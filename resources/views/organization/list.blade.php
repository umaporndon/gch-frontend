@forelse($requestUser as $userItem )
    <div class="pending-box col-8">
        <div class="row align-items-center">
            <div class="col-sm-2">
                <img src="{{ $userItem['image_path'] ? $userItem['image_path'] : asset('images/avatar.png') }}" class="list-thumb">
            </div>
            <div class="col-sm-3">
                <div class="text-bold">{{ $userItem['firstname'] . ' ' . $userItem['lastname'] }}</div>
                <div>{{ $userItem['country'] }}</div>
            </div>
            <div class="col-sm-7">
                <input type="hidden" id="fk_user_id" name="fk_user_id" value="{{ $userItem['id'] }}">
                <input type="hidden" id="id" name="id" value="{{ $userItem['userCompanyID'] }}">
                <input type="hidden" id="status" name="status" value="accept">
                <button onclick="return false;" data-url="{{ route('organization.action-to-join') }}" data-value="accept" class="input-group-text button-green text-bold action-to-join">
                    Accept
                </button>
                <input type="hidden" id="fk_user_id" name="fk_user_id" value="{{ $userItem['id'] }}">
                <input type="hidden" id="userCompanyidID" name="id" value="{{ $userItem['userCompanyID'] }}">
                <input type="hidden" id="status" name="status" value="reject">
                <button onclick="return false;" data-url="{{ route('organization.action-to-join') }}" data-value="reject" class="input-group-text button-red text-bold action-to-join">
                    Reject
                </button>
            </div>
        </div>
    </div>
    @empty
    <div class="pending-box">Not found request.</div>
@endforelse