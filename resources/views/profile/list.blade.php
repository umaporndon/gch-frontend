<div style="border-bottom: 1px solid #005eaa;padding-bottom:20px;">
    <div style="border-bottom: 1px solid #005eaa;padding-top:10px;">
        <h6 class="text-bold">Result</h6>
    </div>
    <div style="padding-top:10px;">
        <h6 class="text-bold">Company</h6>
    </div>
    @forelse($companyList as $companyListItem)
        <div class="row align-items-center">
            <div class="col-sm-5">
                <div>{{ $companyListItem->company_name }}</div>
            </div>
            <div class="col-sm-7">
                <input type="hidden" id="fk_company_id" name="fk_company_id" value="{{ $companyListItem->id }}">
                <div data-url="{{ route('organization.request-to-join') }}"
                     class="button-green pointer request-to-join"
                     style="padding:10 20; font-size:12px;">
                    Join Request
                </div>
            </div>
        </div>

    @empty
        <div class="pending-box">The company not found.</div>
    @endforelse
</div>

<script>
	$( 'body .request-to-join' ).on( 'click', function(){
		$.ajax( {
			        url:     $( this ).data( 'url' ),
			        type:    'POST',
			        data:    { 'fk_company_id': $( 'input[name=fk_company_id]' ).val() },
			        success: function( result, statusText, jqXHR ){
				        if( result.hasOwnProperty( 'message' ) ){

					        $( '#result-box' ).on( 'hidden.bs.modal', function(){
						        if( result.redirectedUrl ){
							        location.href = result.redirectedUrl;
						        } else {
							        form.trigger( 'reset' );
						        }
					        } );

					        Utility.displaySuccessMessageBox( result.message );

				        }
			        },
		        } );
	} );
</script>