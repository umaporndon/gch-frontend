@extends('layouts.app')

@section('page-title', __('profile.page_title.index'))
@section('page-description', __('profile.page_description.index'))

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                @include('layouts.menu')
            </div>
            <div class="col-xs-12 col-md-9">
                <div class="p-2">
                    <div class="page-title">Profile</div>
                    <div class="row pt-2">
                        <div class="col-xs-12 col-md-3">
                            <div class="profile-thumb">

                                <img style="border-radius:50%; width:150px;" src="{{ $user[0]->image_path ? $user[0]->image_path : asset('images/avatar.png') }}" id="preview">
                                <span style="cursor:pointer">
                                    <a class="profile-upload"><i class="fa fa-camera"></i></a>
                                </span>
                                <!-- Modal -->
                                <div class="modal fade bd-example-modal-xl" id="dialog-upload-image" tabindex="-1" role="dialog" aria-labelledby="upload-imageTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="dialog-upload-imageTitle">Image
                                                                                                      preview</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-4 text-center">
                                                        <div id="upload-demo"></div>
                                                    </div>
                                                    <div class="col-sm-4" style="padding:5%;">
                                                        <strong>Select image to crop:</strong>
                                                        <input type="file" id="image">
                                                        <button class="btn btn-success btn-block btn-upload-image"
                                                                style="margin-top:2%"
                                                                data-url="{{ route('profile.update-profile-image', ['id' => $user[0]->id ]) }}">
                                                            Upload Image
                                                        </button>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div id="preview-crop-image" style="background:#8484841f;width:300px;padding:70px 80px;height:300px;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <form action="{{ route('profile.update-profile-image-path', ['id' => $user[0]->id ]) }}"
                                                      method="POST"
                                                      class="submission-form">
                                                    <div class="hide-input"></div>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                        Close
                                                    </button>
                                                    <button type="submit" class="btn btn-primary">Save changes</button>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-9 profile-detail">
                            @if(!$user[0]->firstname)
                                <div>Please update your profile information</div>
                                <div class="text-bold">First Name Middle Name Last Name</div>
                            @else
                                <div class="text-bold">{{ $user[0]->firstname . ' ' . $user[0]->middlename . ' ' . $user[0]->lastname }}</div>
                            @endif

                            <div>{{ $user[0]->email }}</div>
                            <div><span class="text-bold">Wallet Secret:</span>
                                <small>{{ $user[0]['Wallet'] ? $user[0]['Wallet']->wallet_token : '' }}</div>
                            <div><span class="text-bold">Wallet ID:</span> {{ $user[0]['Wallet'] ? $user[0]['Wallet']->wallet_id : '' }}</div>
                        </div>
                    </div>
                    <div>
                        <h5 class="pt-4 text-bold border-bottom-blue">Personal Information</h5>
                        <form action="{{ route('profile.update-profile', ['id' => $user[0]->id]) }}" method="POST" class="submission-form">

                            {{ csrf_field() }}

                            <div class="profile-form col-8">
                                <div class="profile-input">
                                    <label for="gender" class="form-label">Gender</label>
                                    <div>
                                        <input type="radio" name="gender" id="gender" value="Male" {{ $user[0]->gender === 'Male' ? 'checked' : '' }}><span class="pr-3"> Male</span>
                                        <input type="radio" name="gender" id="gender" value="Female" class="pr-3"
                                                {{ $user[0]->gender === 'Female' ? 'checked' : '' }} >
                                        <span class="pr-3">  Female</span>
                                        <input type="radio" name="gender" id="gender" value="Other" class="pr-3"
                                                {{ $user[0]->gender === 'Other' ? 'checked' : '' }}>
                                        <span class="pr-3">  Other</span>
                                    </div>
                                    <p id="gender-help-text" class="help-text help-text none"></p>
                                </div>
                                <div class="profile-input">
                                    <label for="title" class="form-label">Title</label>
                                    <div>
                                        <input type="text" id="title_name" class="form-control"
                                               placeholder="Title (e.g. Mr. , Mrs. , Miss …)" value="{{ $user[0]->title_name }}"
                                               name="title_name">
                                    </div>
                                    <p id="title_name-help-text" class="help-text help-text none"></p>
                                </div>
                                <div class="profile-input">
                                    <label for="title" class="form-label">Name</label>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name="firstname" id="firstname" value="{{ $user[0]->firstname }}" placeholder="First Name">
                                            <p id="firstname-help-text" class="help-text help-text none"></p>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name="middlename" id="middlename" value="{{ $user[0]->middlename }}" placeholder="Middle Name (Optional)">
                                            <p id="middlename-help-text" class="help-text help-text none"></p>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name="lastname" id="lastname" value="{{ $user[0]->lastname }}" placeholder="Last Name">
                                            <p id="lastname-help-text" class="help-text help-text none"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="profile-input">
                                    <label for="address" class="form-label">Address</label>
                                    <div>
                                        <textarea class="form-control" name="address" id="address" placeholder="Address">
                                            {{ $user[0]->address }}
                                        </textarea>
                                        <p id="address-help-text" class="help-text help-text none"></p>
                                    </div>
                                </div>

                                <script src="js/city.js"></script>

                                <div class="profile-input row">
                                    <form>
                                        <div class="col-6">
                                            <label for="country" class="form-label">Country</label>
                                            <select class="form-select form-control" name="country" id="country"
                                                    onchange="set_city_state(this,city_state)">
                                                <option>Open this select country</option>
                                                <script type="text/javascript">setRegions( "{{$user[0]->country}}" );</script>
                                            </select>
                                            <p id="country-help-text" class="help-text help-text none"></p>
                                        </div>
                                        <div class="col-6">
                                            <label for="state" class="form-label">State/City</label>
                                            <select onchange="print_city_state(country,this)" class="form-select form-control" name="city_state" {{--name="state" id="state"--}}>
                                                <option>Open this select state/city</option>
                                                <option value="{{$user[0]->state}}" selected>{{$user[0]->state}}</option>
                                            </select>
                                            <p id="state-help-text" class="help-text help-text none"></p>
                                        </div>
                                        <div id="txtregion" class="d-none"></div>
                                        <div id="txtplacename" class="d-none"></div>
                                    </form>
                                </div>
                                <div class="profile-input row">
                                    {{--<div class="col-6">
                                        <label for="country" class="form-label">City</label>
                                        <select class="form-select form-control" name="city" id="city">
                                            <option>Open this select city</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                        <p id="city-help-text" class="help-text help-text none"></p>
                                    </div>--}}
                                    <div class="col-6">
                                        <label for="country" class="form-label">Postal Code</label>
                                        <input type="text" class="form-control" name="postcode" id="postcode" value="{{ $user[0]->postcode }}" placeholder="Postal Code">
                                        <p id="postcode-help-text" class="help-text help-text none"></p>
                                    </div>
                                </div>
                                <div class="profile-input row">
                                    <div class="col-6">
                                        <label for="country" class="form-label">Phone</label>
                                        <input type="text" class="form-control" name="phone" id="phone" value="{{ $user[0]->phone }}" placeholder="Phone">
                                        <p id="phone-help-text" class="help-text help-text none"></p>
                                    </div>
                                    <div class="col-6">
                                        <label for="country" class="form-label">Mobile</label>
                                        <input type="text" class="form-control" name="mobile" id="mobile" value="{{ $user[0]->mobile }}" placeholder="Mobile">
                                        <p id="mobile-help-text" class="help-text help-text none"></p>
                                    </div>
                                </div>
                                <div class="profile-input">
                                    <label for="title" class="form-label">Email</label>
                                    <div>
                                        <input type="text" class="form-control" name="email" id="email" value="{{ $user[0]->email }}" placeholder="Email Address (e.g. john@example.com)" name="email">
                                        <p id="email-help-text" class="help-text help-text none"></p>
                                    </div>
                                </div>
                                <div class="profile-input">
                                    <label for="title" class="form-label">Second Email</label>
                                    <div>
                                        <input type="text" class="form-control" name="second_email" id="second_email" value="{{ $user[0]->second_email }}" placeholder="Email Address (e.g. john@example.com)l" name="second_semail">
                                        <p id="second_email-help-text" class="help-text help-text none"></p>
                                    </div>
                                </div>
                                <div class="profile-input">
                                    <label for="title" class="form-label">Website</label>
                                    <div>
                                        <input type="text" class="form-control" name="website" id="website" value="{{ $user[0]->website }}" placeholder="Website Address (e.g. www.sample.com)" name="website">
                                        <p id="website-help-text" class="help-text help-text none"></p>
                                    </div>
                                </div>
                            @if($user[0]->kyc_status === null)
                                <!-- This is the Passbase Component -->
                                    <div class="profile-input">
                                        <label for="title" class="form-label">Please verify your KYC/AML account</label>
                                        <div id="passbase-button"></div>
                                    </div>
                                @elseif($user[0]->kyc_status === 'approved')
                                    <div class="kyc-box">
                                        <div class="text-bold">KYC/AML Status</div>
                                        <i class="fa fa-check"></i>Individual
                                    </div>
                                @elseif($user[0]->kyc_status === 'pending')
                                    <div class="kyc-box">
                                        <div class="text-bold">KYC/AML Status</div>
                                        <i class="fa fa-check"></i>Pending For Review
                                    </div>
                                @elseif($user[0]->kyc_status === 'declined')
                                    <div class="kyc-box">
                                        <div class="text-bold">KYC/AML Status</div>
                                        <i class="fa fa-times"></i>Declined
                                    </div>
                                @endif
                                @if( empty( $user_company ) )
                                    <div class="profile-input">
                                        <label for="title" class="form-label">Join existing company</label>
                                        <div class="input-group">
                                            <input type="text" class="search-text form-control mr-1" style="border-radius:0.25rem" placeholder="Company" name="company">
                                            <a data-url="{{ route('organization.getcompany') }}" class="input-group-text button-blue pt-1 pointer" id="search-company">Search</a>
                                        </div>
                                    </div>
                                @else
                                    <div style="border-bottom: 1px solid #005eaa;padding-bottom:20px;">
                                        <div style="border-bottom: 1px solid #005eaa;padding-top:10px;">
                                            <h6 class="text-bold">Joining Status</h6>
                                        </div>
                                        <div style="padding-top:10px;">
                                            <h6 class="text-bold">Company</h6>
                                        </div>
                                        @foreach( $user_company as $item )
                                            <div class="row align-items-center pt-2">
                                                <div class="col-sm-5">
                                                    <div>{{ $item['company_name'] }}</div>
                                                </div>
                                                <div class="col-sm-7">
                                                    <button onclick="return false;" class="button-yellow pointer request-to-join"
                                                            style="padding:10 20; font-size:12px;">
                                                        Pending
                                                    </button>
                                                    <input type="hidden" id="fk_company_id" name="fk_company_id" value="{{ $item['company_id'] }}">
                                                    <input type="hidden" id="id" name="id" value="{{ $item['id'] }}">
                                                    <input type="hidden" id="status" name="status" value="cancel">
                                                    <button onclick="return false;" data-url="{{ route('organization.cancel-to-join') }}" class="button-gray pointer action-to-join"
                                                            style="padding:10 20; font-size:12px;">
                                                        Cancel
                                                    </button>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                                <div id="company-search-result"></div>
                                <div class="profile-input mt-3">
                                    <div class="input-group">
                                        <a href="{{ route('organization.index') }}" class="input-group-text button-blue text-bold p-2 w-50">Add
                                                                                                                                            New
                                                                                                                                            Company
                                        </a>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="input-group-text button-save w-75 text-bold" type="submit">Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
		// This is the logic for the passbase component
		const element = document.getElementById( 'passbase-button' );
		const apiKey  = "{{ env('MIX_PASSBASE_PUBLISHABLE_KEY') }}";

		Passbase.renderButton( element, apiKey, {
			onFinish: ( identityAccessKey ) => {
				console.log( 'Verification completed with identityAccessKey: ', identityAccessKey );
			},
			onError:  ( errorCode ) => {
				alert(errorCode);
				console.log( 'Error: ', errorCode );
			},
			onStart:  () => {
			},
		} );

		// You can prefill your user's email here to skip the step. Left empty for demo
		const userEmail = '';

		Passbase.renderButton( element, apiKey, {
			prefillAttributes: {
				email: userEmail,
			},
		} );
    </script>

@endsection
