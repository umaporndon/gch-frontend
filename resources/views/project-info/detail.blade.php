@extends('layouts.app')

@section('page-title', __('project-info.page_title.index'))
@section('page-description', __('project-info.page_description.index'))

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                @include('layouts.menu')
            </div>
            <div class="col-xs-12 col-md-9">
                <div class="p-2">
                    <div class="page-title">Project Info</div>
                    <h4>{{ $projectInfoDetail[0]->name }}</h4>
                    @if( $image !== null )
                        <img src="data:image/png;base64,{{ $image }}" style="width:100%">
                    @endif
                    <div class="content">
                        {!! $projectInfoDetail[0]->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
