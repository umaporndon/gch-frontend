@extends('layouts.app')

@section('page-title', __('project-info.page_title.index'))
@section('page-description', __('project-info.page_description.index'))

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                @include('layouts.menu')
            </div>
            <div class="col-xs-12 col-md-9">
                <div class="p-2">
                    <div class="page-title">Project Info</div>
                    <table class="table project-table">
                        <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Country</th>
                            <th scope="col">Type</th>
                            <th scope="col">Standard</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($projectInfoList as $item)
                            <tr data-url="{{ route('project-info.detail', [ 'id' => $item->id ]) }}" class="table-link">
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->country }}</td>
                                <td>{{ $item->type }}</td>
                                <td>{{ $item->standard }}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection
