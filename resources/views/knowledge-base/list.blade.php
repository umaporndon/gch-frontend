@foreach( $data['knowledgeBaseList'] as $item )
    <div class="col-4">
        <div data-url="{{ route('knowledge-base.detail', ['id' => $item->id ]) }}" class="table-link">
            <div class="kb-content kb-box-{{$loop->index}}">
                <div class="kb-thumb top-50 start-50">
                    @if( $item->new_image_path !== null )
                        <img style="width:100px; height:100px; border-radius: 50%;"
                             src="data:image/png;base64,{{ $item->new_image_path }}">
                    @endif
                </div>
                <div class="kb-title">{{$item->title}}</div>
                <div class="kb-desc">{{$item->description}}</div>
            </div>
        </div>
    </div>
@endforeach