@extends('layouts.app')

@section('page-title', __('knowledge-base.page_title.index'))
@section('page-description', __('knowledge-base.page_description.index'))

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                @include('layouts.menu')
            </div>
            <div class="col-xs-12 col-md-9">
                <div class="p-2">
                    <div class="page-title p-1">Knowledge Base</div>
                    <h4>{{ $knowledgeBaseDetail[0]->title }}</h4>
                    @if( $image !== null )
                        <img src="data:image/png;base64,{{ $image }}" style="width:100%">
                    @endif
                    <div class="content">
                        {!! $knowledgeBaseDetail[0]->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
