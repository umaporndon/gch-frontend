@extends('layouts.app')

@section('page-title', __('knowledge-base.page_title.index'))
@section('page-description', __('knowledge-base.page_description.index'))

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                @include('layouts.menu')
            </div>
            <div class="col-xs-12 col-md-9">
                <div class="p-2">
                    <div class="page-title p-1">Knowledge Base</div>
                    <div class="row">
                        <div class="col-sm text-center kb-top">
                            <div class="kb-white">How can we help you today?</div>
                            <div class="row pt-3">
                                <div class="col-md-8 offset-md-2 search-box">
                                    <form id="search-form-detail" class="search" method="GET"
                                          action="{{ route('knowledge-base.index') }}">
                                        {{ csrf_field() }}
                                        <div class="input-group mb-3">
                                           <input class="form-control search-text" name="search" placeholder="Find answer">
                                            <button  type="submit" class="button input-group-text search-button">Search
                                                <i class="fa fa-search"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-between row pt-4" id="search-result">
                        @include('knowledge-base.list')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
