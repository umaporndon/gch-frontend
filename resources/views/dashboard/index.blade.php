@extends('layouts.app')

@section('page-title', __('dashboard.page_title.index'))
@section('page-description', __('dashboard.page_description.index'))

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                @include('layouts.menu')
            </div>
            <div class="col-xs-12 col-md-9">
                <div class="p-2">
                    <div class="page-title">Dashboard</div>
                    <div class="pt-2 content">
                        <p>Welcome to the Token Management System for carbon credits.</p>

                        <p>To offset your carbon credits, please transfer them to the offset address.
                           We will automatically arrange for the carbon credits to be removed from the National
                           Register on behalf of your organization and send you the documents.</p>

                        <p> <b>Offset (Burn) Address:</b></br>
                        L2LKSJPDN2BPKP5SDQPEBCG5KMXYDMIAI3WO43BH5GPRE5ZIBTVQFIKN34
                        </p>
                    </div>
                    <div style="padding-top:30px;">
                        <h3>ESG@ACX</h3>
                        <table class="table dash-table">
                            <tr>
                                <th>Current</th>
                                <th>US$</th>
                                <th>8.75</th>
                            </tr>
                            <tr>
                                <th>7d Low</th>
                                <th>US$</th>
                                <th>7.48</th>
                            </tr>
                            <tr>
                                <th>7d High</th>
                                <th>US$</th>
                                <th>8.93</th>
                            </tr>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
