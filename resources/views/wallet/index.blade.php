@extends('layouts.app')

@section('page-title', __('wallet.page_title.index'))
@section('page-description', __('wallet.page_description.index'))

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                @include('layouts.menu')
            </div>
            <div class="col-xs-12 col-md-9">
                <div class="p-2">
                    <div class="page-title">Wallet</div>
                    <div class="pt-1">
                        <a class="w-50 login-button text-uppercase text-center"
                           href="https://wallet.myalgo.com/newaccount" target="_blank">
                            Access Your Wallet
                        </a>
                    </div>
                    <div class="pt-1">
                        <a onclick="connectAlgoSigner()" class="w-50 button-connect text-uppercase text-center" id="connect-to-algoSigner">
                            Connect To AlgoSigner
                        </a>
                        <div class="text-red" id="error-code"></div>
                    </div>
                    <div style="padding-top:60px;">
                        <h3>AirCarbon (ACX) Balances</h3>
                        <table class="table balance-table">
                            <tr>
                                <th>Symbol</th>
                            </tr>
                            @foreach( $data->result as $balance )
                                <tr>
                                    <td>{{ $balance->symbol }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="dialog-recommendation" tabindex="-1" role="dialog" aria-labelledby="dialog-recommendation" aria-hidden="true">
        <div class="modal-dialog modal-dialog-top" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="dialog-upload-imageTitle">Recommendation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row p-3">
                        <div>AlgoSigner is NOT installed.</div>
                        <div>Please install
                            <a class="text-bold"
                               href="https://chrome.google.com/webstore/detail/algosigner/kmmolakhbgdlpkjkcjkebenjheonagdm"
                               target="_blank">
                                &nbsp;Algorand Wallet Extension&nbsp;
                            </a>
                             on your google chrome.
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
		function connectAlgoSigner(){

			if( typeof AlgoSigner !== 'undefined' ){

				window.console.log( 'AlgoSigner is installed.' );

				AlgoSigner.connect()
				          .then( ( d ) => {
					          window.console.log( JSON.stringify( d ) );
					          //var algoAccount = AlgoSigner.accounts( { ledger: 'TestNet' } );
					          //window.console.log( algoAccount );
					          window.$( '#error-code' ).hide();
				          } )
				          .catch( ( e ) => {
					          console.error( e );
					          document.getElementById( 'error-code' ).innerHTML = JSON.stringify( e.message );
					          window.$( '#error-code' ).show();
				          } );

			} else {
				window.console.log( 'AlgoSigner is NOT installed.' );
				window.$( '#dialog-recommendation' ).modal( 'show' );
			}
		}

    </script>
@endsection
