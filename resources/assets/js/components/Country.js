/**
 * @namespace
 * @desc Handles country.
 */
const Country = (function(){
	/**
	 * @memberOf Country
	 * @access public
	 * @desc Initialize Country module.
	 */
	function initialize(){
		$( '.table-link' ).on( 'click', function(){
			let url       = $( this ).data( 'url' );
			location.href = url;
		} );
	}

	return {
		initialize: initialize,
	};
})( jQuery );
