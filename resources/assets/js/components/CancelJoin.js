/**
 * @namespace
 * @desc Handles cancelJoin.
 */
const CancelJoin = (function(){
	/**
	 * @memberOf CancelJoin
	 * @access public
	 * @desc Initialize CancelJoin module.
	 */
	function initialize(){
		$( 'body .cancel-to-join' ).on( 'click', function(){

			$.ajax( {
				        url:     $( this ).data( 'url' ),
				        type:    'POST',
				        data:    {
					        'fk_company_id': $( 'input[name=fk_company_id]' ).val(),
					        'status':        $( 'input[name=status]' ).val(),
					        'id':            $( 'input[name=id]' ).val(),
				        },
				        success: function( result, statusText, jqXHR ){

					        if( result.hasOwnProperty( 'message' ) ){

						        $( '#result-box' ).on( 'hidden.bs.modal', function(){
							        if( result.redirectedUrl ){
								        location.href = result.redirectedUrl;
							        } else {
								        form.trigger( 'reset' );
							        }
						        } );

						        Utility.displaySuccessMessageBox( result.message );

					        }
				        },
			        } );
		} );

		$( 'body .action-to-join' ).on( 'click', function(){

			$.ajax( {
				        url:     $( this ).data( 'url' ),
				        type:    'POST',
				        data:    {
					        'status':     $( 'input[name=status]' ).val(),
					        'id':         $( 'input[name=id]' ).val(),
				        },
				        success: function( result, statusText, jqXHR ){

					        if( result.hasOwnProperty( 'message' ) ){

						        $( '#result-box' ).on( 'hidden.bs.modal', function(){
							        if( result.redirectedUrl ){
								        location.href = result.redirectedUrl;
							        } else {
								        form.trigger( 'reset' );
							        }
						        } );

						        Utility.displaySuccessMessageBox( result.message );

					        }
				        },
			        } );
		} );

	}

	return {
		initialize: initialize,
	};
})( jQuery );
