/**
 * @namespace
 * @desc Handles table link management.
 */
const TableLink = (function(){
	/**
	 * @memberOf TableLink
	 * @access public
	 * @desc Initialize TableLink module.
	 */
	function initialize(){
		$( '.table-link' ).on( 'click', function(){
			let url       = $( this ).data( 'url' );
			location.href = url;
		} );
	}

	return {
		initialize: initialize,
	};
})( jQuery );
