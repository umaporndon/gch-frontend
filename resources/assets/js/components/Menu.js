/**
 * @namespace
 * @desc Handles menu management.
 */
const Menu = (function() {
  /**
   * @memberOf Menu
   * @access public
   * @desc Initialize Menu module.
   */
  function initialize() {
  }

  return {
    initialize: initialize,
  };
})(jQuery);
