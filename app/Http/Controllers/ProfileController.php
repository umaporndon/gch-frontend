<?php
/**
 * Profile Page Controller
 */

namespace App\Http\Controllers;

use App\Models\InterestIn;
use App\Models\OrganizationCategory;
use App\Models\UserInterestIn;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;
use App\Models\Users;
use App\Models\Wallet;

/**
 * Profile Page Controller
 * @package App\Http\Controllers
 */
class ProfileController extends Controller
{

    /** @var Users User model */
    protected $usersModel;

    /**
     * Initialize RegisterController class.
     *
     * @param Users $users Users model
     */
    public function __construct( Users $users )
    {
        $this->usersModel = $users;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $datas
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator( array $data )
    {
        return Validator::make( $data, [
            'email'     => 'required',
            'firstname' => 'required',
            'lastname'  => 'required',
        ] );
    }

    /**
     * Display profile page.
     *
     * @return Factory|View profile page
     */
    public function index()
    {
        $user         = $this->usersModel->getUserProfile();
        $user_company = $this->usersModel->getUserCompany();

        return view( 'profile.index', compact( 'user', 'user_company' ) );
    }

    /**
     * Update the authenticated user's profile.
     *
     * @param Request $request HTTP request object
     *
     * @return \Illuminate\Http\JsonResponse Update response
     */
    public function updateProfile( Request $request )
    {
        $this->validator( $request->input() )->validate();

        $response = $this->usersModel->updateUser( $request );

        if( !$response['success'] ){
            return response()->json( $response, 422 );
        }

        return response()->json( [
                                     'success'       => $response['success'],
                                     'message'       => $response['message'],
                                     'redirectedUrl' => url()->previous(),
                                 ] );
    }

    public function updateProfileImage( Request $request )
    {
        $response = $this->usersModel->saveImage( $request );

        return response()->json( $response );
    }

    public function updateProfileImagePath( Request $request )
    {
        $response = $this->usersModel->updateProfileImage( $request );

        if( !$response['success'] ){
            return response()->json( $response, 422 );
        }

        return response()->json( [
                                     'success'       => $response['success'],
                                     'message'       => $response['message'],
                                     'redirectedUrl' => url()->previous(),
                                 ] );

    }

    public static function encode_personal_link()
    {
        $yourPassbaseLink = "https://verify.passbase.com/mathias";

        $hash_map = array(
            'prefill_attributes' => array(
                'email' => 'testuser+34567865456@passbase.com',
                'country' => 'de'
            ),
        );

        $encodedAttributes = base64_encode(json_encode($hash_map));
        $encodedLink = $yourPassbaseLink."?p=".$encodedAttributes;
        return $encodedLink;
    }
}
