<?php
/**
 * Wallet Page Controller
 */

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use App\Libraries\ServiceRequest;

/**
 * Wallet Page Controller
 * @package App\Http\Controllers
 */
class WalletController extends Controller
{
    /**
     * Display wallet page.
     *
     * @return Factory|View Wallet page
     */
    public function index()
    {

        $client = new Client();
        try{


            $response = $client->request( 'GET', env( 'AIR_CARBON_BASE_URI' ) . 'pub/pairs',
                                          [
                                              'headers' => [
                                                  'Authorization' => env( 'AIR_CARBON_API_KEY' ),
                                              ],
                                          ],);
            $body     = $response->getBody();
            $data     = json_decode( $body );
            $status   = true;

        } catch( ClientException $ce ) {
            $status   = false;
            $response = json_decode( $ce->getResponse()->getBody()->getContents() );

            $data = [];
        }

        return view( 'wallet.index', compact( 'data' ) );
    }
}
