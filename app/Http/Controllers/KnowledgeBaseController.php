<?php
/**
 * Knowledge Base Page Controller
 */

namespace App\Http\Controllers;

use App\Libraries\ServiceRequest;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use App\Models\KnowledgeBase;
use Illuminate\Http\Request;

/**
 * Knowledge Base Page Controller
 * @package App\Http\Controllers
 */
class KnowledgeBaseController extends Controller
{
    /** @var KnowledgeBase KnowledgeBase model */
    protected $knowledgeBaseModel;

    /**
     * Initialize KnowledgeBaseController class.
     *
     * @param KnowledgeBase $knowledgeBase Users model
     */
    public function __construct( KnowledgeBase $knowledgeBase )
    {
        $this->knowledgeBaseModel = $knowledgeBase;
    }

    /**
     * Display knowledge base page.
     *
     * @return Factory|View knowledge base page
     */
    public function index( Request $request )
    {
        $data['knowledgeBaseList'] = $this->knowledgeBaseModel->getKnowledgeBaseList( $request );

        if( $request->ajax() ){
            return response()->json( [
                                         'data' => view( 'knowledge-base.list', compact( 'data' ) )->render(),
                                     ] );
        }

        return view( 'knowledge-base.index', compact( 'data' ) );
    }

    public function detail( $id )
    {
        $knowledgeBaseDetail = $this->knowledgeBaseModel->getKnowledgeBaseDetail( $id );
        $image               = null;

        if( $knowledgeBaseDetail[0]->image_path !== null ){
            $image = ServiceRequest::call(
                'GET',
                '/assets/' . $knowledgeBaseDetail[0]->image_path,
                true,
            );
        }

        return view( 'knowledge-base.detail', compact( 'knowledgeBaseDetail', 'image' ) );
    }
}
