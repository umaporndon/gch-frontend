<?php
/**
 * Home Page Controller
 */

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Illuminate\Http\Request;

/**
 * Home Page Controller
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Display home page.
     *
     * @return Factory|View Home page
     */
    public function index(Request $request)
    {
        return redirect()->route('dashboard.index');
    }
}
