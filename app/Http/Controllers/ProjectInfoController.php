<?php
/**
 * Project Information Page Controller
 */

namespace App\Http\Controllers;

use App\Models\Users;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use App\Models\ProjectInfo;
use App\Libraries\ServiceRequest;

/**
 * Project Information Page Controller
 * @package App\Http\Controllers
 */
class ProjectInfoController extends Controller
{
    /** @var ProjectInfo ProjectInfo model */
    protected $projectInfoModel;

    /**
     * Initialize ProjectInfoController class.
     *
     * @param Users $projectInfo Users model
     */
    public function __construct( ProjectInfo $projectInfo )
    {
        $this->projectInfoModel = $projectInfo;
    }

    /**
     * Display project information page.
     *
     * @return Factory|View Project information page
     */
    public function index()
    {
        $projectInfoList = $this->projectInfoModel->getProjectInformationList();

        return view( 'project-info.index', compact( 'projectInfoList' ) );
    }

    public function detail( $id )
    {
        $projectInfoDetail = $this->projectInfoModel->getProjectInformationDetail( $id );
        $image             = null;

        if( $projectInfoDetail[0]->image_path !== null ){
            $image = ServiceRequest::call(
                'GET',
                '/assets/' . $projectInfoDetail[0]->image_path,
                true,
            );
        }

        return view( 'project-info.detail', compact( 'projectInfoDetail', 'image' ) );
    }
}
