<?php
/**
 * Organization Page Controller
 */

namespace App\Http\Controllers;

use App\Libraries\Search;
use App\Models\Company;
use App\Models\UsersCompany;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;
use Illuminate\Http\Request;

/**
 * Organization Page Controller
 * @package App\Http\Controllers
 */
class OrganizationController extends Controller
{
    protected $companyModel;
    protected $usersCompanyModel;

    /**
     * Initialize CompanyController class.
     *
     * @param Users $company Company model
     */
    public function __construct( Company $company, UsersCompany $usersCompany )
    {
        $this->companyModel      = $company;
        $this->usersCompanyModel = $usersCompany;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $datas
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator( array $data )
    {
        return Validator::make( $data, [
            'email'        => 'required',
            'company_name' => 'required',
        ] );
    }

    /**
     * Display organization page.
     *
     * @return Factory|View organization page
     */
    public function index()
    {
        $requestUser   = [];
        $companyMember = [];
        $company       = $this->companyModel->getCompanyProfile();

        if( !isset( $company ) ){

            $requestUser   = $this->companyModel->getRequestUser( $company );
            $companyMember = $this->companyModel->getCompanyMember( $company );
        }

        return view( 'organization.index', compact( 'company', 'requestUser', 'companyMember' ) );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param Request $request User data
     *
     * @return Users ReferralUsers model
     */
    protected function create( Request $request )
    {
        $this->validator( $request->input() )->validate();

        $response = $this->companyModel->createCompany( $request );

        return response()->json( [
                                     'success'       => $response['success'],
                                     'message'       => $response['message'],
                                     'redirectedUrl' => url()->previous(),
                                 ] );
    }

    public function updateCompanyImage( Request $request )
    {
        $response = $this->companyModel->saveImage( $request );

        return response()->json( $response );
    }

    public function updateCompanyImagePath( Request $request )
    {
        $response = $this->companyModel->updateCompanyImage( $request );

        if( !$response['success'] ){
            return response()->json( $response, 422 );
        }

        return response()->json( [
                                     'success'       => $response['success'],
                                     'message'       => $response['message'],
                                     'redirectedUrl' => url()->previous(),
                                 ] );

    }

    public function getCompany( Request $request )
    {
        $builder = $this->companyModel->where( [ 'status' => 'active' ] )
                                      ->orderBy( 'id', 'desc' );

        $companyList = Search::search( $builder, 'company', $request );

        if( $request->ajax() ){
            return response()->json( [
                                         'data' => view( 'profile.list', compact( 'companyList' ) )->render(),
                                     ] );
        }
    }

    public function requestToJoin( Request $request )
    {
        $response = $this->usersCompanyModel->createUsersCompany( $request );

        return response()->json( [
                                     'success'       => $response['success'],
                                     'message'       => $response['message'],
                                     'redirectedUrl' => url()->previous(),
                                 ] );
    }

    public function cancelToJoin( Request $request )
    {
        $response = $this->usersCompanyModel->updateUsersCompany( $request );

        return response()->json( [
                                     'success'       => $response['success'],
                                     'message'       => $response['message'],
                                     'redirectedUrl' => url()->previous(),
                                 ] );
    }

    public function actionToJoin( Request $request )
    {
        $response = $this->usersCompanyModel->updateUsersCompany( $request );

        return response()->json( [
                                     'success'       => $response['success'],
                                     'message'       => $response['message'],
                                     'redirectedUrl' => url()->previous(),
                                 ] );
    }

}
