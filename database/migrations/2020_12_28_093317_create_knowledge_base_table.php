<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKnowledgeBaseTable extends Migration
{
    const Table = 'knowledge_base';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( self::Table, function( Blueprint $table ){
            $table->increments( 'id' );
            $table->unsignedInteger('fk_user_id')->nullable();
            $table->foreign( 'fk_user_id' )->references( 'id' )->on( 'users' );
            $table->string('title', 255);
            $table->text('description')->nullable();
            $table->text('content')->nullable();
            $table->string('image_path', 255);
            $table->string('status', 255);
            $table->timestamp( 'updated_at' )->nullable();
            $table->timestamp( 'created_at' )->nullable();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( self::Table );
    }
}
