<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
    const Table = 'company';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( self::Table, function( Blueprint $table ){
            $table->increments( 'id' );
            $table->unsignedInteger( 'fk_wallet_id' )->nullable();
            $table->foreign( 'fk_wallet_id' )->references( 'id' )->on( 'wallet' );
            $table->unsignedInteger( 'fk_user_id' )->nullable();
            $table->foreign( 'fk_user_id' )->references( 'id' )->on( 'users' );
            $table->string( 'company_name', 255 )->nullable();
            $table->string( 'email', 255 )->nullable();
            $table->string( 'second_email', 255 )->nullable();
            $table->text( 'address' )->nullable();
            $table->string( 'country', 255 )->nullable();
            $table->string( 'state', 255 )->nullable();
            $table->string( 'city', 255 )->nullable();
            $table->string( 'postcode', 255 )->nullable();
            $table->string( 'phone', 20 )->nullable();
            $table->string( 'mobile', 20 )->nullable();
            $table->string( 'website', 255 )->nullable();
            $table->string( 'image_path', 255 )->nullable();
            $table->string( 'status', 255 );
            $table->string( 'kyc_status', 255 )->nullable();
            $table->timestamp( 'updated_at' )->nullable();
            $table->timestamp( 'created_at' )->nullable();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( self::Table );
    }
}
